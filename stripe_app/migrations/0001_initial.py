# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Charge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('charge_id', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('amount', models.IntegerField()),
                ('amount_refunded', models.IntegerField()),
                ('captured', models.BooleanField()),
                ('created', models.DateTimeField()),
                ('failture_code', models.CharField(max_length=255)),
                ('paid', models.BooleanField()),
                ('plan_name', models.CharField(max_length=255)),
                ('status', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
