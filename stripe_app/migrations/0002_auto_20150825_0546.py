# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stripe_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='charge',
            old_name='failture_code',
            new_name='failure_code',
        ),
    ]
