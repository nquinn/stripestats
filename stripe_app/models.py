from django.db import models

# Create your models here.


#(charge.id, charge.description, charge.amount , charge.amount_refunded 
#charge.refunded, charge.captured , charge.created , 
#charge.dispute , charge.failure_code , charge.paid , 
#charge.refunded , 
#charge.statement_descriptor, charge.status)

class Charge(models.Model):
    charge_id = models.CharField(max_length=255)
    description = models.TextField()
    amount = models.IntegerField()
    amount_refunded = models.IntegerField()
    captured = models.BooleanField()
    created = models.DateTimeField()
    failure_code = models.CharField(max_length=255)
    paid = models.BooleanField()
    plan_name = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    # dispute = this is an object
    
    
    
