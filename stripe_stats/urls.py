from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponse

def hello(request):
    return HttpResponse("Hello World")

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'stripe_stats.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'$', hello)
)
