import stripe
import json

stripe.api_key = "sk_test_uNxoTfbPCZiMl85mi5g4BOp4"

# create customer
# email, source, metadata(key/values)

def create_customer():
    stripe.Customer.create(
        email = "neil_stripe@mailinator.com",
        description="customer description here"
    )


# add card
# 4000000000000077 - success
# 4000000000000002 - card declined   

# create a charge
# amount, currency, source or customer

'''
def create_charge():
    stripe.Charge.create(
    amount=12,
    currency="usd",
    description="Charge test",
    customer="cus_6qnHAh5NlhGVsR"
    )
'''    


# sample charge id:  ch_16d5v5G7tIGue5ojV3NGuuEz
def show_charge(charge_id):
    charge_object = stripe.Charge.retrieve(charge_id)
    d = charge_object.to_dict()
    print (d)

def list_charges():
    charges = stripe.Charge.all(limit=10) # returns an object of objects
    for charge in charges.data:
        #print (charge.id, charge.description, charge.amount , charge.refunded, charge.captured , charge.created , charge.dispute , charge.failure_code , charge.paid , charge.refunded , charge.statement_descriptor, charge.status)
        refund_total = 0
        for refund in charge.refunds.data:
            refund_total += charge.amount
        print (charge.amount,refund_total)
        #refunded - True = fully refunded, but can miss partials, "refunds" has all
        
def list_customers():
    stripe.Customer.all()

#show_charge()
list_charges()